<!DOCTYPE html>
<html>
<head>
	<title></title>
	@extends('layout.app')
</head>
<body>
	<h3>Edit Buku</h3> <br>

	<a href="/bukku"> Kembali</a> <br>
	
	<br/>
	<br/>

	@foreach($buku as $b)
	<form action="/bukku/update" method="post">
	{{ csrf_field() }}
	<input type="hidden" name="id" value="{{ $b->id }}"> <br/>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Judul Buku</label>
			{{ csrf_field() }}
			<input type="text" class="form-control col-sm-4" required="required" name="buku_judul" value="{{ $b->buku_judul }}" >
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Penulis Buku</label>
			{{ csrf_field() }}
			<input type="text" class="form-control col-sm-4" required="required" name="buku_penulis" value="{{ $b->buku_penulis }}" >
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Penerbit Buku</label>
			{{ csrf_field() }}
			<input type="text" class="form-control col-sm-4" required="required" name="buku_penerbit" value="{{ $b->buku_penerbit }}" >
		</div>
		<input type="submit" value="Simpan Data">
	</form>
	@endforeach
		

</body>
</html>