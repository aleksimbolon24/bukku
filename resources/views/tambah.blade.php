<!DOCTYPE html>
<html>
<head>
	<title></title>
	@extends('layout.app')
</head>
<body>
 
	<h3>Data Buku</h3> <br>
 
	<a href="/bukku"> Kembali</a> <br>
	
	<br/>
 
	<form action="/bukku/store" method="post">
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Judul Buku</label>
			{{ csrf_field() }}
			<input type="text" class="form-control col-sm-4" required="required" name="buku_judul" >
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Penulis Buku</label>
			{{ csrf_field() }}
			<input type="text" class="form-control col-sm-4" required="required" name="buku_penulis"  >
		</div>
		<div class="form-group row">
			<label for="formGroupExampleInput" class="col-sm-2 col-form-label">Penerbit Buku</label>
			{{ csrf_field() }}
			<input type="text" class="form-control col-sm-4" required="required" name="buku_penerbit"  >
		</div>
		<input type="submit" value="Simpan Data">
	</form>
 
</body>
</html>