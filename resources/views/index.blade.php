<!DOCTYPE html>
<html>
<head>
	<title></title>
	@extends('layout.app')
</head>
<body>

	<style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style>

	<h3>Data Buku</h3>


	<a href="/bukku/tambah"> + Tambah Buku</a>
	<table class="table table-bordered">
		<thead>
			<tr>
			<th scope="col">Id</th>
			<th scope="col">Judul</th>
			<th scope="col">Penulis</th>
			<th scope="col">Penerbit</th>
			</tr>
		</thead>
		@foreach($buku as $b)
		<tr>
		<td scope="row"> {{ $loop->iteration }} </td>
			<td>{{ $b->buku_judul }}</td>
			<td>{{ $b->buku_penulis }}</td>
			<td>{{ $b->buku_penerbit }}</td>
			<td>
				
				<a class="btn-warning btn-sm" href="/crud/edit/{{ $b->id }}">Edit</a>|
				<a onclick="return confirm('Yakin ingin mengapus?')" class="btn btn-danger btn-sm" href="/crud/hapus/{{ $b->id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>

	<br/>

	{{ $buku->links() }}

	

</body>
</html>