<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/bukku','BookController@index');

Route::get('/bukku/tambah','BookController@tambah');

Route::post('/bukku/store','BookController@store');

Route::get('/bukku/edit/{id}','BookController@edit');

Route::post('/bukku/update','BookController@update');

Route::get('/bukku/hapus/{id}','BookController@hapus');

